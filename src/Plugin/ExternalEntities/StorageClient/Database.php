<?php

namespace Drupal\xnttdb\Plugin\ExternalEntities\StorageClient;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Database\IntegrityConstraintViolationException;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\Query\Sql\Condition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\dbxschema\Database\DatabaseTool;
use Drupal\dbxschema\Exception\ConnectionException;
use Drupal\dbxschema\Exception\DatabaseToolException;
use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\ExternalEntityTypeInterface;
use Drupal\external_entities\Plugin\PluginFormTrait;
use Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface;
use Drupal\external_entities\StorageClient\ExternalEntityStorageClientBase;
use Drupal\xnttdb\Exception\DatabaseExternalEntityException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * External entities storage client for external databases/schemas.
 *
 * @ExternalEntityStorageClient(
 *   id = "xnttdb",
 *   label = @Translation("Database"),
 *   description = @Translation("Retrieves database records.")
 * )
 */
class Database extends ExternalEntityStorageClientBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Default schema name identifier.
   */
  const DEFAULT_SCHEMA = '<default>';

  /**
   * Available cross connections.
   *
   * @var array
   */
  protected static $xConnections;

  /**
   * The (cross) connection to use.
   *
   * @var \Drupal\dbxschema\Database\CrossSchemaConnectionInterface
   */
  protected $xConnection;

  /**
   * Placeholder data.
   *
   * Keys are placeholder names with schema prefix and values are replacement
   * values.
   *
   * @var array
   */
  protected $placeholders;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Database tool service.
   *
   * @var \Drupal\dbxschema\Database\DatabaseTool
   */
  protected $databaseTool;

  /**
   * Database object identifier field name.
   *
   * @var string
   */
  protected $databaseIdField;

  /**
   * Constructs a database external storage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface $response_decoder_factory
   *   The response decoder factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger service (xnttdb).
   * @param \Drupal\dbxschema\Database\DatabaseTool $database_tool
   *   Database tool service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    ResponseDecoderFactoryInterface $response_decoder_factory,
    EntityFieldManagerInterface $entity_field_manager,
    MessengerInterface $messenger,
    CacheBackendInterface $cache,
    LoggerChannelInterface $logger,
    ?DatabaseTool $database_tool
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $response_decoder_factory
    );

    // Services injection.
    $this->entityFieldManager = $entity_field_manager;
    $this->messenger = $messenger;
    $this->cache = $cache;
    $this->logger = $logger;
    $this->databaseTool = $database_tool;

    // Get database identifier field for database queries.
    if (!empty($configuration['_external_entity_type'])) {
      try {
        $field_mapper = $configuration['_external_entity_type']->getFieldMapper();
        $this->databaseIdField = $field_mapper->getFieldMapping('id')['value'] ?? 'id';
      }
      catch (PluginNotFoundException $e) {
        // The external entity type has not been saved yet. Do nothing.
      }
    }
    $this->initXConnection($configuration);
  }

  /**
   * Initialize externalEntityType member if not set.
   */
  protected function initExternalEntityType(FormStateInterface $form_state) {
    // Get current external entity type if not set.
    if (empty($this->externalEntityType)) {
      $state_data = $form_state->getStorage();
      $xntt_type_id = $state_data['machine_name.initial_values']['id'];
      if (!empty($xntt_type_id)) {
        $xntt_type = \Drupal::entityTypeManager()
          ->getStorage('external_entity_type')
          ->load($xntt_type_id)
        ;
        $this->externalEntityType = $xntt_type;
      }
    }
  }

  /**
   * Internal function to initialize connection.
   *
   * @param array $configuration
   *   A configuration array.
   * @param bool $reload_cache
   *   If TRUE, it reloads (identifier) cache.
   */
  protected function initXConnection(
    array $configuration,
    bool $reload_cache = FALSE
  ) {
    // Gets schema and default schema.
    $schemas = $configuration['connection']['schemas'] ?? [];
    $default_schema = array_shift($schemas);
    // Get database key if one.
    $dbkey = $configuration['connection']['dbkey'] ?? 'default';
    if (!empty($default_schema) && !empty($this->databaseTool)) {
      $ckey = $dbkey . '#' . $default_schema . '#' . implode('#', $schemas);
      // Try to get connection from static connection pool.
      if (!empty(static::$xConnections[$ckey])) {
        $this->xConnection = static::$xConnections[$ckey];
        try {
          // Initializes placeholders replacement.
          $this->initPlaceholders($reload_cache);
        }
        catch (DatabaseExternalEntityException $e) {
          $this->messenger->addError(
            'Database External Entity initialization failed: ' . $e->getMessage()
          );
        }
      }
      else {
        // Initializes connection, first schema and extra-schemas.
        try {
          if (empty($default_schema)
              || (strtolower($default_schema) == static::DEFAULT_SCHEMA)
          ) {
            $default_schema = '';
          }
          // Get an external schema connection.
          try {
            $connection = $this->databaseTool->getConnection($default_schema, $dbkey);
          }
          catch (ConnectionException $e) {
            throw new DatabaseExternalEntityException(
              'Failed to get a valid connection. ' . $e->getMessage()
            );
          }
          // Initialize connection.
          if (!$connection->schema()->schemaExists()) {
            throw new DatabaseExternalEntityException(
              'Invalid schema "'
              . $default_schema
              . '". The schema does not exist.'
            );
          }
          // Get related database tool (instead of default provided one).
          $db_tool = $this->databaseTool = $connection->getDatabaseTool();
          // Adds extra schemas.
          foreach ($schemas as $schema_name) {
            if (!$db_tool->schemaExists($schema_name)) {
              throw new DatabaseExternalEntityException(
                'Invalid schema "'
                . $schema_name
                . '". The schema does not exist.'
              );
            }
            $connection->addExtraSchema($schema_name);
          }
          static::$xConnections[$ckey] = $this->xConnection = $connection;

          // Initializes placeholders replacement.
          $this->initPlaceholders($reload_cache);
        }
        catch (DatabaseExternalEntityException | DatabaseToolException | PluginException $e) {
          $this->messenger->addError(
            'Database External Entity initialization failed: ' . $e->getMessage()
          );
        }
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $messenger = $container->get('messenger');
    try {
      $db_tool = $container->get('dbxschema.tool');
    }
    catch (DatabaseToolException | PluginException $e) {
      // DatabaseToolException: Invalid argument passed to DBTool.
      // PluginException: Driver not found.
      $messenger->addError(
        'XNTTDB: ' . $e->getMessage()
      );
    }
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('external_entities.response_decoder_factory'),
      $container->get('entity_field.manager'),
      $messenger,
      $container->get('cache.default'),
      $container->get('logger.channel.xnttdb'),
      $db_tool
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'qcreate' => NULL,
      'qread'   => NULL,
      'qupdate' => NULL,
      'qdelete' => NULL,
      'qlist'   => NULL,
      'qcount'  => NULL,
      'connection'   => [
        // A Drupal setting.php database key.
        'dbkey' => '',
        // An array of schema name strings or an empty array for default schema.
        'schemas' => [],
      ],
      // Structure: array of [
      //   'placeholder' => string placeholder name,
      //   // Either 'query' OR 'constant' key but not both.
      //   'query' => string SQL query to return corresponding value
      //   'constant' => constant value
      // ]
      'placeholders' => [],
      // Structure: array which keys are Drupal fields and values are SQL
      // mappings.
      'filter_mappings' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    parent::setConfiguration($configuration);
    // Update placeholders.
    $this->initXConnection($configuration, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $this->initExternalEntityType($form_state);

    $placeholder_div_id = uniqid('xnttdb');
    $filtermap_div_id = uniqid('xnttdb');
    // Database and schemas selection.
    $form_state->setCached(FALSE);
    $db_settings_open = $form_state->get('db_settings_open') ??
      (
        !empty($this->configuration['connection']['schemas'][0])
        && ($this->configuration['connection']['schemas'][0] != static::DEFAULT_SCHEMA)
      );
    $form['connection'] = [
      '#type' => 'details',
      '#title' => $this->t('Connection settings'),
      '#open' => $db_settings_open,
    ];
    $form['connection']['dbkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary database key name (optional)'),
      '#description' => $this->t(
        "Leave this field empty if you don't know how to use it or if you are
        not using another database than the Drupal one. Otherwise, you can use
        the database \"key\" used to specify another database in your Drupal site
        \"settings.php\" file."
      ),
      '#default_value' => $this->configuration['connection']['dbkey'],
    ];
    $form['connection']['description'] = [
      '#type' => 'markup',
      '#markup' =>
      '<div>'
      . $this->t(
        "You may specify one or more schemas if you don't want to use
        just the default one."
      )
      . '</div>',
    ];
    $schema_count = $form_state->get('schema_count');
    if (empty($schema_count)) {
      $schema_count = count($this->configuration['connection']['schemas']) + 1;
    }
    $form_state->set('schema_count', $schema_count);

    for ($i = 0; $i < $schema_count; ++$i) {
      $value = $this->configuration['connection']['schemas'][$i] ?? '';
      $form['connection']['schemas'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Schema name'),
        '#default_value' => $value,
      ];
    }
    $form['connection']['add_schema'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add a schema'),
      '#name' => 'add_schema',
      '#ajax' => [
        'callback' => [
          get_class($form_state->getFormObject()),
          'buildAjaxStorageClientConfigForm',
        ],
        'wrapper' => 'external-entities-storage-client-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    // Interface for placeholder queries.
    // Fields: 'placeholder', 'query'.
    $placeholders_open = $form_state->get('placeholders_open') ?? FALSE;
    $form['placeholder_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Placeholders'),
      '#open' => $placeholders_open,
      '#prefix' => '<div id="' . $placeholder_div_id . '">',
      '#suffix' => '</div>',
    ];
    $form['placeholder_settings']['description'] = [
      '#markup' =>
      '<div>'
      . $this->t(
        'You can define placeholders that will be replaced by cached values in
        your queries. Thoses values are fetched using the provided SQL query
        when the database external entity type is saved or when the cache
        needs to be updated. Therefore, you can use those placeholders in your
        SQL queries below and avoid running subqueries or additional joins to
        your original query in order to get a given value that relies on
        static data.'
      )
      . '</div><div>'
      . $this->t('For each placeholder, you must specify the name of the placeholder to use in queries (including leading ":"). It must start with ":" followed by a letter and only contain alphanumeric characters and underscores. You can leave it empty to remove a placeholder setting.')
      . '</div><div>'
      . $this->t('You must also specify a SQL query to use to fetch the value(s). The query must return only one column. Additionnal columns will be ignored. Multiple rows are supported but the placeholder should be suffixed with "[]" (eg.: ":multi_ids[]").')
      . '</div>'
      ,
    ];
    $placeholder_count = $form_state->get('placeholder_count');
    if (empty($placeholder_count)) {
      $placeholder_count = count($this->configuration['placeholders']) + 1;
    }
    $form_state->set('placeholder_count', $placeholder_count);

    for ($i = 0; $i < $placeholder_count; ++$i) {
      $value = $this->configuration['placeholders'][$i] ?? FALSE;
      // Try to get a sample value.
      $current_value = NULL;
      if (!empty($value['placeholder'])) {
        $current_value = $this->replacePlaceholders($value['placeholder']);
        // Check if it has been replaced.
        if ($current_value == $value['placeholder']) {
          $current_value = NULL;
        }
      }

      $form['placeholder_settings']['placeholders'][$i] = [
        '#type' => 'fieldset',
        'placeholder' => [
          '#type' => 'textfield',
          '#title' => $this->t('Placeholder'),
          '#title_display' => 'before',
          '#default_value' => ($value ? $value['placeholder'] : ''),
        ],
      ];
      if (isset($value['constant']) && empty($value['query'])) {
        // Use constant when set and no query.
        $form['placeholder_settings']['placeholders'][$i]['constant'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Constant value'),
          '#title_display' => 'before',
          '#default_value' => ($value ? $value['constant'] : ''),
          '#description' => !empty($current_value) ? $this->t('Current value: @current_value', ['@current_value' => $current_value]) : '',
          // If we need to disable editing on constants (or use an hidden field
          // instead):
          // '#disabled' => TRUE,
          // '#attributes'=> ['readonly' => 'readonly',],
        ];
      }
      else {
        // Use a 'query' as default.
        $form['placeholder_settings']['placeholders'][$i]['query'] = [
          '#type' => 'textarea',
          '#title' => $this->t('SQL query'),
          '#title_display' => 'before',
          '#default_value' => ($value ? $value['query'] : ''),
          '#description' => !empty($current_value) ? $this->t('Current value: @current_value', ['@current_value' => $current_value]) : '',
        ];
      }
    }
    $form['placeholder_settings']['add_placeholder'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add a placeholder'),
      '#name' => 'add_placeholder',
      '#ajax' => [
        'callback' => [
          get_class($form_state->getFormObject()),
          'buildAjaxStorageClientConfigForm',
        ],
        'wrapper' => 'external-entities-storage-client-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    // We must keep "queries" as a tree structure event if we flatten it later
    // because we are in a subform and setting #tree to FALSE would make the
    // query values disapear when form_state will flatten the data as they would
    // appear in the parent form_state but not in this children sub-form_state
    // provided for validation.
    $form['queries'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CRUD + List/Count'),
      '#tree' => TRUE,
    ];
    $form['queries']['crud_description'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'CRUD (<b>C</b>reate <b>R</b>ead <b>U</b>pdate <b>D</b>elete) +
        List/Count SQL queries are used to manage database objects.<br/>
        Only 3 queries are required: READ, LIST and COUNT. Leaving others
        empty would just disable their actions.<br/>
        You should specify tables using the notation
        "<b>{#:<i>tablename</i>}</b>" where "#" is a schema index starting from
        1 and corresponding to the schema specified above (just use "1" if you
        don\'t know). You may also specify Drupal tables if needed using either
        the regular Drupal notation "{tablename}" or {0:tablename}. You may also
        use placeholders in queries as defined
        <a href="#@placeholder_div_id">above</a>.',
        ['@placeholder_div_id' => $placeholder_div_id,]
      ),
    ];

    $form['queries']['qcreate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CREATE: SQL queries to create a new object'),
      '#description' => $this->t(
        'Ex. (PostgreSQL): INSERT INTO {1:foo} VALUES (:id_field, :other_field_name, :yet_another_field_name); SELECT currval(\'&lt;name_id_seq&gt;\') AS "id";'
      ),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qcreate'],
    ];
    $form['queries']['qcreate_help'] = [
      '#type' => 'details',
      '#title' => $this->t('CREATE Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qcreate_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'Each query <b>must contain the fields</b> it suppors using placeholders
        ":&lt;field_machine_name&gt;".<br/> For instance, to insert a stock that
        has "name"="My Stock", "uniquename"="MYSTOCK001" and
        "type_name"="stock_types:my type" (using the format
        "cv.name:cvterm.name") fields mapped, use:<br/>
        <code>INSERT INTO {1:stock} (name, uniquename, type_id) SELECT :name,
        :uniquename, cvterm_id FROM {1:cvterm} cvt JOIN {1:cv} cv USING (cv_id)
        WHERE cvt.name = substr(:type_name, strpos(:type_name, \':\') + 1) AND
        cv.name = substr(:type_name, 1, strpos(:type_name, \':\') - 1) LIMIT 1;
        </code><br/>
        The <b>last query MUST return</b> the created item identifier value
        aliased as "id". For PostgreSQL, it would be the last sequence
        value:<br/>
        <code>SELECT currval(\'&lt;name_id_seq&gt;\') AS id;</code><br/>
        and for MySQL it would be:<br/>
        <code>SELECT last_insert_id() AS id;</code>'
      ),
    ];


    $form['queries']['qread'] = [
      '#type' => 'textarea',
      '#title' => $this->t('READ: SQL queries to get a full object'),
      '#description' => $this->t(
        'Ex.: SELECT id_field, other_field_name, field3 AS "yet_another_field_name" FROM {1:foo} WHERE id_field = :id;'
      ),
      '#required' => TRUE,
      '#default_value' => $this->configuration['qread'],
    ];
    $form['queries']['qread_help'] = [
      '#type' => 'details',
      '#title' => $this->t('READ Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qread_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'The query can be complex with joins and will return the wanted fields
        named using "AS" clause (otherwise, the table field names will be used).
        Returned field names starting with "<i>array_</i>" will be parsed as
        database arrays and will be turned into array of values (ie. mapping
        fields should have a cardinality greater than 1) while field names
        starting with "<i>json_</i>" will be parsed as JSON data and returned
        as structured objects.
        The query <b>must contain a placeholder ":id"</b> refering to the object
        identifier which field is aliased as "id". The ":id" placeholders used
        in queries must be used as right operands with either "= :id" or
        "IN (:id)" operators.
        Fields names should be distinct, contain only alphanumeric and
        underscore characters and must not start with a number.<br/>
        Ex.: <code>SELECT s.stock_id AS "id", s.name AS "name", s.uniquename AS
        "uniquename", cv.name || \':\' || cvt.name AS "type_name" FROM {1:stock}
        s JOIN {1:cvterm} cvt ON cvt.cvterm_id = s.type_id JOIN {1:cv} cv ON
        cv.cv_id = cvt.cv_id WHERE s.stock_id = :id;</code>.<br/>
        When multiple queries are used, new field values are added to the entity
        and existing ones are replaced by the last queries.'
      ),
    ];

    $form['queries']['qupdate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('UPDATE: SQL queries to update an existing object'),
      '#description' => $this->t(
        'Ex.: UPDATE {1:foo} SET id_field = :id_field, other_field_name = :other_field_name, field3 = :yet_another_field_name WHERE id_field = :id;'
      ),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qupdate'],
    ];
    $form['queries']['qupdate_help'] = [
      '#type' => 'details',
      '#title' => $this->t('UPDATE Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qupdate_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'Multiple queries are allowed separated by ";". Each query <b>must
        contain the fields</b> it supports using placeholders
        ":&lt;field_machine_name&gt;".<br/>
        For instance, to update a stock that has "name", "uniquename" and
        "type_name" (using the format "cv.name:cvterm.name") fields mapped,
        use:<br/>
        <code>UPDATE {1:stock} SET name = :name, uniquename = :uniquename,
        type_id = (SELECT :name, :uniquename, cvterm_id FROM {1:cvterm} cvt JOIN
        {1:cv} cv USING (cv_id) WHERE s.stock_id = :stock_id AND cvt.name =
        substr(:type_name, strpos(:type_name, \':\') + 1) AND cv.name =
        substr(:type_name, 1, strpos(:type_name, \':\') - 1) LIMIT 1);</code>'
      ),
    ];

    $form['queries']['qdelete'] = [
      '#type' => 'textarea',
      '#title' => $this->t('DELETE: SQL queries to delete an object'),
      '#description' => $this->t(
        'Ex.: DELETE FROM {1:foo} WHERE id_field = :id;'
      ),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qdelete'],
    ];
    $form['queries']['qdelete_help'] = [
      '#type' => 'details',
      '#title' => $this->t('DELETE Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qdelete_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'Each query must contain the fields it needs using placeholders
        ":<field_machine_name>". There should be the identifier field followed
        by the "= :id" placeholder.<br/>
        For instance, to delete a given stock, use:<br/>
        <code>DELETE FROM {1:stock} WHERE stock_id = :id;</code>'
      ),
    ];

    $form['queries']['qlist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('LIST: SQL query to list objects'),
      '#description' => $this->t(
        'Ex.: SELECT id_field, other_field_name, field3 AS "yet_another_field_name" FROM {1:foo} WHERE TRUE :filters'
      ),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qlist'],
    ];
    $form['queries']['qlist_help'] = [
      '#type' => 'details',
      '#title' => $this->t('LIST Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qlist_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'There must be only <b>one statement</b> for listing. The query <b>must
        return object IDs and names</b> and <b>must NOT include "ORDER BY",
        "LIMIT" and "OFFSET" clauses</b> which will be appended automatically to
        the query by the system. It should include a placeholder ":filters" (in
        a WHERE clause) to indicate where filters could be added by the system
        in order to filter the list. Since the list query may be used without
        filters, the system should automatically remove the "WHERE" keyword if
        it is followed by nothing. If it fails for your specific query, consider
        adding a simple "TRUE" keyword after the "WHERE":<br/>
        <code>... WHERE TRUE :filters;</code><br/>
        When you use aliased columns (ie. SELECT ... <u>AS "..."</u>) you must
        specify their mapping in the
        "<a href="#@filtermap_div_id">Filter mapping overrides</a>"
        section below. For instance, if you used <code>SELECT ... field3 AS
        "yet_another_field_name"...</code> in the READ query, you will have to
        add a mapping for the field "yet_another_field_name" that will contain
        the value "field3".',
        ['@filtermap_div_id' => $filtermap_div_id,]
      ),
    ];

    $form['queries']['qcount'] = [
      '#type' => 'textarea',
      '#title' => $this->t('COUNT: SQL query to count objects'),
      '#description' => $this->t(
        'Ex.: SELECT COUNT(1) FROM {1:foo} WHERE TRUE :filters'
      ),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qcount'],
    ];
    $form['queries']['qcount_help'] = [
      '#type' => 'details',
      '#title' => $this->t('COUNT Help'),
      '#open' => FALSE,
    ];
    $form['queries']['qcount_help']['details'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        'The COUNT query must follow the same restrictions as the LIST query,
        the only difference is that the query must return just one integer as
        "count".'
      ),
    ];

    $form['filter_mappings'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter mapping overrides'),
      '#open' => FALSE,
      '#description' => $this->t('In order to allow filtering, Drupal field
        names need to be associated with their corresponding SQL column names
        or expressions. Most of those mapping will be guessed by the system so
        you could leave those mapping empty however in some cases, you may have
        to specify the mapping.<br/>
        For instance, if your READ query looks like this:
        <code>SELECT ..., cv.name || \':\' || cvt.name AS "type_name", ... FROM ...</code>
        you will have to specify the mapping value
        "<code>cv.name || \':\' || cvt.name</code>"
        for the field name that is mapped to the storage value "type_name" (after saving
        the external entity field mapping and reloading this page).
        <br/>
        Currently, this only works with simple Drupal fields (those with only
        one column, as opposed to compound fields, such as latitude/longitude)
        mapped using the simple mapper.
        <br/>
        You may need to reload this page after saving external entity field
        mapping changes in order to see those fields in the filter mapping
        section.
      '),
      '#prefix' => '<div id="' . $filtermap_div_id . '">',
      '#suffix' => '</div>',
    ];

    if (!empty($this->externalEntityType)
        && ($this->externalEntityType instanceof ExternalEntityTypeInterface)
    ) {
      $entity_type = $this->externalEntityType->getDerivedEntityTypeId();
      $field_definitions = $this
        ->entityFieldManager
        ->getFieldDefinitions($entity_type, $entity_type);
      foreach ($field_definitions as $field_machine_name => $field_definition) {
        if (strpos($field_machine_name, 'annotation') === 0) {
          // Annotations are not relevant.
          continue;
        }

        $form['filter_mappings'][$field_machine_name] = [
          '#type' => 'textfield',
          '#title' => $this->t('@human_name (@machine_name)', [
            '@human_name' => $field_definition->getLabel(),
            '@machine_name' => $field_machine_name,
          ]),
          '#default_value' => $this->configuration['filter_mappings'][$field_machine_name] ?? '',
        ];
      }
    }

    // Add debug option at the end of the form.
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug SQL queries'),
      '#description' => $this->t('Note: curly braces and square brackets in logged queries are replaced by UTF8 equivalent characters. Do not forget to replace them if you plan to use logged queries somewhere else.'),
      '#default_value' => $this->configuration['debug'] ?? FALSE,
      '#weight' => 1000,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * The validation expects 4 values from the $form_state:
   * - 'connection': it must be an aray like this: [
   *     'dbkey' => 'a database key or an empty string',
   *     'schemas' => [ an array of schema names as strings ],
   *   ]
   * - 'queries': it must be an array like this: [
   *     'qcreate' => 'SQL query/ies or empty string',
   *     'qread' =>
   *       'non-empty SQL query/ies with *... = :id* or *... IN (:id[])*',
   *     'qupdate' => 'SQL query/ies or empty string',
   *     'qdelete' => 'SQL query/ies or empty string',
   *     'qlist' => 'single SQL query with *:filters*',
   *     'qcount' => 'single SQL query with *... AS "count"* and *:filters*',
   *   ]
   * - 'placeholder_settings': it must be an array like this: [
   *     [
   *       'placeholders'=> [
   *         'placeholder' => ':placeholder_name',
   *         // Either 'query' or 'constant' key but not both.
   *         'query' => 'SQL query to fetch the placeholder value',
   *         'constant' => 'A constant value to use',
   *       ],
   *       ...
   *     ],
   *   ]
   * - 'filter_mappings': it must be an array like this: [
   *     some_field_machine_name =>
   *       'the corresponding SQL expression to map that field',
   *     ...
   *   ]
   *
   * Note: there is no ::submitConfigurationForm because of the PluginForm
   * system. All is done here.
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->initExternalEntityType($form_state);
    // Check for Ajax events.
    if ($trigger = $form_state->getTriggeringElement()) {
      switch ($trigger['#name']) {
        case 'add_schema':
          $schema_count = $form_state->get('schema_count');
          $form_state->set('schema_count', $schema_count + 1);
          $form_state->set('db_settings_open', TRUE);
          $form_state->setRebuild(TRUE);
          break;

        case 'add_placeholder':
          $placeholder_count = $form_state->get('placeholder_count');
          $form_state->set('placeholder_count', $placeholder_count + 1);
          $form_state->set('placeholders_open', TRUE);
          $form_state->setRebuild(TRUE);
          break;

        default:
      }
    }

    // Check query structures (single/multitple), presence of ':id', etc.
    $queries = $form_state->getValue('queries', []);
    $queries += [
      'qcreate' => '',
      'qread'   => '',
      'qupdate' => '',
      'qdelete' => '',
      'qlist'   => '',
      'qcount'  => '',
    ];
    if (empty($queries['qread'])) {
      // Non-empty string but empty value.
      $queries['qread'] = '0';
    }
    // Try to auto-generate missing mandatory queries.
    if (empty($queries['qlist'])) {
      // Generate LIST from READ.
      $qlist = $queries['qread'];
      if (1 === preg_match_all('/(^|\W)SELECT\W/is', $qlist)) {
        // Remove ":id" filter.
        $qlist = preg_replace('/(?:AND\s+)?(?:[\w\.]+\s*(?:=|IN)\s*(?::id(?:\[\])?|\(:id(?:\[\])?\))|:id\s*(?:=|IN)\s*(?::?[\w\.]+|\(:?[\w\.]+\)))(?:\s|$)/is', ' ', $qlist);
        // If first condition, remove the extra "AND".
        $qlist = preg_replace('/WHERE\s+AND\s/is', 'WHERE ', $qlist);
        $qlist = rtrim($qlist, "; \n\r\t\v\x00");
        if (!preg_match('/\W:id\W/', $qlist)) {
          if (preg_match('/\WWHERE(?:\W|$)\s*(.*\w.*|)$/is', $qlist, $match)) {
            if (empty($match[1])) {
              $qlist .= ' TRUE :filters';
            }
            else {
              $qlist .= ' :filters';
            }
          }
          else {
            $qlist .= ' WHERE TRUE :filters';
          }
          $message = 'The LIST query was empty. An auto-generated one has been set but might not work. You may have to fix it manually. ';
          $this->messenger->addWarning($message);
          $this->logger->warning($message);
          $queries['qlist'] = $qlist;
          $form_state->set('qlist', $qlist);
        }
      }
    }
    if (!empty($queries['qlist']) && empty($queries['qcount']) ) {
      $qcount = $queries['qlist'];
      $qcount = preg_replace('/^\s*SELECT\s.+?\sFROM\s/is', 'SELECT COUNT(1) AS "count" FROM ', $qcount);
      if ($qcount != $queries['qlist']) {
        $message = 'The COUNT query was empty. An auto-generated one has been set but might not work. You may have to fix it manually.';
        $this->messenger->addWarning($message);
        $this->logger->warning($message);
        $queries['qcount'] = $qcount;
        $form_state->set('qcount', $qcount);
      }
    }

    if (!preg_match('/(?:=|\WIN|\Win)\s*\(?:id(?:\W|$)/', $queries['qread'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qread']);
      $form_state->setErrorByName('queries][qread', 'The READ query must contain a "= :id" placeholder.');
    }
    if (empty($queries['qread'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qread']);
      $form_state->setErrorByName('queries][qread', 'The READ query is mandatory.');
    }
    if (empty($queries['qlist'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qlist']);
      $form_state->setErrorByName('queries][qlist', 'The LIST query is mandatory (and could not be auto-generated from the READ query).');
    }
    if (empty($queries['qcount'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qcount']);
      $form_state->setErrorByName('queries][qcount', 'The COUNT query is mandatory (and could not be auto-generated from the LIST query).');
    }
    if (preg_match('/\w.*;.*\w/', $queries['qlist'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qlist']);
      $form_state->setErrorByName('queries][qlist', 'The LIST query must not contain more than one query.');
    }
    if (preg_match('/\w.*;.*\w/', $queries['qcount'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qcount']);
      $form_state->setErrorByName('queries][qcount', 'The COUNT query must not contain more than one query.');
    }
    if (!preg_match('/\Wcount\s*\(|\s"?count"?\s/is', $queries['qcount'])) {
      // Get value to let FormState know the element we complain about.
      $form_state->getValue(['queries', 'qcount']);
      $form_state->setErrorByName('queries][qcount', 'The COUNT query must return a column named/aliased "count".');
    }
    // Check if the fields were filled to avoid displaying unecessary warnings.
    if (preg_match('/\w/', $queries['qlist']) && !preg_match('/:filters(?:\W|$)/', $queries['qlist'])) {
      $message = 'The LIST query does not contain a ":filters" placeholder. If the LIST query fails, add a ":filters" placeholder to fix it.';
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }
    if (preg_match('/\w/', $queries['qcount']) && !preg_match('/:filters(?:\W|$)/', $queries['qcount'])) {
      $message = 'The COUNT query does not contain a ":filters" placeholder. If the LIST query fails, add a ":filters" placeholder to fix it.';
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }
    if (preg_match('/\w/', $queries['qcreate']) && !preg_match('/(^\s*|;\s*)SELECT\s(?!.*;\s*UPDATE\W).*$/is', $queries['qcreate'])) {
      $message = 'The CREATE query does not seem to return the created entity identifier. Verify the query.';
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }
    if (preg_match('/\w/', $queries['qupdate']) && !preg_match('/\W:id(?:\W|$)/', $queries['qupdate'])) {
      $message = 'The UPDATE query does not contain a ":id" placeholder. It may update more than the expected entity. Verify the query.';
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }
    if (preg_match('/\w/', $queries['qdelete']) && !preg_match('/\W:id(?:\W|$)/', $queries['qdelete'])) {
      $message = 'The DELETE query does not contain a ":id" placeholder. It may delete more than the expected entity. Verify the query.';
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }

    $form_state->setValue('qcreate', $queries['qcreate'] ?? '');
    $form_state->setValue('qread', $queries['qread'] ?? '');
    $form_state->setValue('qupdate', $queries['qupdate'] ?? '');
    $form_state->setValue('qdelete', $queries['qdelete'] ?? '');
    $form_state->setValue('qlist', $queries['qlist'] ?? '');
    $form_state->setValue('qcount', $queries['qcount'] ?? '');
    // We don't want to save queries twice.
    $form_state->unsetValue('queries');

    $form_state->setValue(
      'filter_mappings',
      $form_state->getValue('filter_mappings', [])
    );

    $schemas = array_filter(
      $form_state->getValue(['connection', 'schemas'], [])
    );
    $dbkey = $form_state->getValue(['connection', 'dbkey'], '');
    $form_state->setValue('connection', [
      'dbkey' => $dbkey,
      'schemas' => $schemas,
    ]);

    // Only keep fully filled values.
    $placeholders = $form_state->getValue(
      ['placeholder_settings'],
      ['placeholders' => []]
    )['placeholders'];
    $placeholders = array_filter(
      $placeholders,
      function ($item) {
        return !empty($item['placeholder']) && (!empty($item['query']) || isset($item['constant']));
      }
    );
    // Add placeholders prefix if missing.
    $valid_placeholders = [':id' => TRUE, ':id[]' => TRUE, ':filters' => TRUE, ];
    foreach ($placeholders as $index => $placeholder) {
      $placeholders[$index]['placeholder'] = preg_replace(
        '#^:*(?=[a-zA-Z])#', ':',
        $placeholder['placeholder']
      );
      $valid_placeholders[$placeholders[$index]['placeholder']] = TRUE;
    }
    $form_state->setValue('placeholders', $placeholders);
    // We don't need 'placeholder_settings' (redondancy).
    $form_state->unsetValue('placeholder_settings');
    // Make sure every ':placeholder' in queries has been defined.
    $missing_placeholders = [];
    $query_types = [
      'qcreate' => 'CREATE query',
      'qread'   => 'READ query',
      'qupdate' => 'UPDATE query',
      'qdelete' => 'DELETE query',
      'qlist'   => 'LIST query',
      'qcount'  => 'COUNT query',
    ];
    foreach ($query_types as $query_type => $query_name) {
      if (preg_match_all('/[^\w:](:\w+(?:\[\])?)(?:\W|$)/', $queries[$query_type], $matches)) {
        foreach ($matches[1] as $match) {
          if (!array_key_exists($match, $valid_placeholders)) {
            $missing_placeholders[$match] = $query_type;
          }
        }
      }
    }
    if (!empty($missing_placeholders)) {
      $message = $this->t(
        'The following placeholders are used in queries but are not defined: %placeholders',
        ['%placeholders' => implode(', ', array_keys($missing_placeholders)), ]
      );
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
    }
    $this->setConfiguration($form_state->getValues());

    // Reinit-placeholders.
    $this->initPlaceholders(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(ExternalEntityInterface $entity) {
    $entity_data = $entity->toRawData();
    $success = $this->runQueries($this->configuration['qdelete'], $entity_data);
    if (empty($success)) {
      $this->messenger->addError(
        'Failed to delete entity ' . $entity->id()
      );
    }
    return SAVED_DELETED;
  }

  /**
   * Expands a database array of a field value.
   *
   * Database driver used returns array field results as strings starting with
   * '{' and ending with '}' which values are separated by comas ",".
   *
   * @param string|array $sql_array
   *   A field value containing a database array.
   *
   * @return array
   *   The array of values.
   */
  protected function expandDatabaseArray($sql_array) :array {
    $new_array = [];
    if (is_string($sql_array)) {
      if ((0 < strlen($sql_array))
        && ('{' === $sql_array[0])
        && ('}' === substr($sql_array, -1))
      ) {
        // Valid (PostgreSQL) array syntax so far.
        $sql_array = substr($sql_array, 1, -1);
        $i = 0;
        $current_value = '';
        $current_set = &$new_array;
        $parent_set = [];
        while ($i < strlen($sql_array)) {
          if ('"' === $sql_array[$i]) {
            // Get quoted value.
            ++$i;
            $current_value = '';
            while (($i < strlen($sql_array)) && ('"' !== $sql_array[$i])) {
              $current_value .= $sql_array[$i++];
              if (($i < strlen($sql_array)) && ('\\' === $sql_array[$i-1])) {
                // Escape character, skip next.
                $current_value .= $sql_array[$i++];
              }
            }
            ++$i;
            $current_set[] = stripslashes($current_value);
          }
          elseif ('{' === $sql_array[$i]) {
            // Start a sub-array.
            ++$i;
            $current_value = '';
            $parent_set[] = &$current_set;
            $current_set[] = [];
            $current_set = &$current_set[count($current_set)-1];
          }
          elseif ('}' === $sql_array[$i]) {
            // End a sub-array.
            ++$i;
            $current_value = '';
            $current_set = &$parent_set[count($parent_set)-1];
            array_pop($parent_set);
          }
          elseif (',' === $sql_array[$i]) {
            // Comma separation.
            $current_value = '';
            ++$i;
          }
          elseif (' ' === $sql_array[$i]) {
            // space.
            ++$i;
          }
          else {
            // Get non-quoted value.
            $current_value = '';
            while (($i < strlen($sql_array)) && (',' !== $sql_array[$i]) && ('}' !== $sql_array[$i])) {
              $current_value .= $sql_array[$i++];
            }
            $current_set[] = $current_value;
          }
          // Skip spaces.
          while (($i < strlen($sql_array)) && (' ' === $sql_array[$i])) {
            ++$i;
          }
        }
      }
      else {
        // Turn invalid string into an array of one element.
        $new_array[] = $sql_array;
      }
    }
    elseif (is_array($sql_array)) {
      // Already an array, maybe the driver already handles arrays.
      $new_array = $sql_array;
    }

    return $new_array;
  }

  /**
   * Expands a database JSON of a field value.
   *
   * Expected $sql_json is just a string representation of a JSON structure.
   *
   * @param string $sql_json
   *   A string field value containing a JSON structure.
   * @param string $info
   *   Optional additionnal info to log in case of decoding error.
   * @return
   *   The JSON structure in PHP representation.
   */
  protected function expandDatabaseJson($sql_json, string $info = '') {
    $json = Json::decode($sql_json);
    if (NULL === $json) {
      if (!empty($this->externalEntityType)) {
        $info =
          ' for '
          . $this->externalEntityType->getDerivedEntityTypeId()
          . " ($info)"
        ;
      }
      $this->logger->warning(
        "Failed to parse JSON data $info:\n"
        . print_r($sql_json, TRUE)
        . "\nError:\n"
        . json_last_error_msg()
      );
      // Fallback to raw value.
      $json = $sql_json;
    }
    return $json;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $data = [];

    if (empty($this->xConnection)) {
      // No connection available.
      $this->messenger->addError('Multiple loading failed: no connection available.');
      return $data;
    }

    try {
      // Replaces "=:id" or "IN(:id)" with "IN(:id[])".
      // Note: replacing !=:id, >=:id and <=:id is not supported.
      $queries = preg_replace(
        ['/(?<![!<>])=\s*:id(\W|$)/', '/(\W)IN\s*\(?:id(?:\[\])?\)?(\W|$)/is'],
        [' IN (:id[])\1', '\1IN (:id[])\2'],
        $this->configuration['qread']
      );
      // Make sure $queries does not contain any invalid ':id' left.
      if (preg_match('/\W:id(?:[^\[a-zA-Z_]|$)/', $queries)) {
        // There are still some ":id" left.
        $this->logger->error("Failed to replace all \":id\" with \"IN (:id[])\". Query: " . $queries);
      }
      $sql_queries = [];
      // @todo better parse ';' as it can be used in query strings.
      $sql_queries = explode(';', $queries);
      // Loop on queries.
      foreach ($sql_queries as $sql_query) {
        $sql_query = trim($sql_query);
        if (empty($sql_query)) {
          continue;
        }
        $qargs = [':id[]' => $ids] + $this->getPlaceholders($sql_query);
        if (!empty($this->configuration['debug'])) {
          $this->log("READ SQL Query:\n$sql_query\n\nArguments:\n" . print_r($qargs, TRUE));
        }
        // Fetch corresponding data in database.
        $results = $this->xConnection->query(
          $sql_query,
          $qargs
        )->fetchAllAssoc($this->databaseIdField ?? 'id', \PDO::FETCH_ASSOC);

        // Restructures PostgreSQL arrays.
        foreach ($results as $id => $element) {
          foreach ($element as $key => $value) {
            if (0 === strpos($key, 'array_')) {
              $data[$id][$key] = $this->expandDatabaseArray($value);
            }
            elseif (0 === strpos($key, 'json_')) {
              $info = "(id:$id, field:$key)";
              $data[$id][$key] = $this->expandDatabaseJson($value, $info);
            }
            else {
              $data[$id][$key] = $value;
            }
          }
        }
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }
    catch (IntegrityConstraintViolationException $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }
    if (empty($data)) {
      $data = [];
    }
    return $data;
  }

  /**
   * Loads one entity.
   *
   * @param mixed $id
   *   The ID of the entity to load.
   *
   * @return array|null
   *   A raw data array, NULL if no data returned.
   */
  public function load($id) {
    return current($this->loadMultiple([$id])) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    // Check if ID exists in base.
    $entity_data = $entity->toRawData();
    if ($entity->id()) {
      $updated_entity = $this->runQueries(
        $this->configuration['qupdate'], $entity_data
      );
      if (empty($updated_entity)) {
        $this->messenger->addError(
          'Failed to update entity ' . $entity->id()
        );
      }
      else {
        $result = SAVED_UPDATED;
      }
    }
    else {
      $new_entity = $this->runQueries(
        $this->configuration['qcreate'], $entity_data
      );
      if (empty($new_entity)) {
        $this->messenger->addError('Failed to create new entity.');
      }
      else {
        $result = $new_entity['id'] ?? SAVED_NEW;
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function query(
    array $parameters = [],
    array $sorts = [],
    $start = NULL,
    $length = NULL
  ) {
    if (empty($this->xConnection)) {
      // No connection available.
      $this->messenger->addError('Query failed: no connection available.');
      return [];
    }

    try {
      $start = $start ?? 0;
      $order_clause = '';
      $mapped_sorts = [];
      foreach ($sorts as $sort) {
        $mapped_field = $this->getFilterMapping($sort['field']) ?: NULL;
        if (empty($mapped_field)) {
          $this->logger->warning(
            'Missing filter mapping for field "' . $sort['field'] . '"'
          );
        }
        else {
          $mapped_sorts[] =
            $mapped_field
            . ' '
            . $sort['direction']
          ;
        }
      }
      $order_clause = empty($mapped_sorts)
        ? ''
        : ' ORDER BY ' . implode(',', $mapped_sorts)
      ;
      list($filters, $fvalues) = $this->translateParameters($parameters);
      $query = rtrim($this->configuration['qlist'], ';');
      // Check if we have filters.
      if (empty($filters)) {
        // No filters, remove any empty 'WHERE' structure.
        $query = preg_replace('/\sWHERE\s+:filters\s*$/is', '', $query);
      }
      else {
        // Check if the query contains the ":filters" placeholder.
        if (FALSE === strpos($query, ':filters')) {
          // No ":filters", try to append.
          if (preg_match('/\sWHERE\s/is', $query)) {
            $query .= ' :filters';
          }
          else {
            $query .= ' WHERE :filters';
          }
        }
      }
      $query = str_replace(':filters', $filters, $query);
      $query .=
        $order_clause
        . (!empty($length) ? ' LIMIT ' . $length : '')
        . (!empty($start) ? ' OFFSET ' . $start : '')
      ;
      $qargs = $this->getPlaceholders($query);
      if (!empty($this->configuration['debug'])) {
        $this->log("LIST SQL Query:\n$query\n\nArguments:\n" . print_r($fvalues + $qargs, TRUE));
      }
      $results = $this->xConnection->query(
        $query,
        $fvalues + $qargs
      )->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (DatabaseExceptionWrapper $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }
    catch (IntegrityConstraintViolationException $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }

    return $results ?? [];
  }

  /**
   * Translates the string operators to SQL equivalents.
   *
   * @param array $parameters
   *   An array of condition arrays.
   *
   * @return array
   *   Returns an array of 2 values: the first one is a SQL condition statement
   *   and the second value is an array of replacement values for the
   *   conditions.
   *
   * @note this only works for fields whose placeholder names directly match SQL
   * column names or have a correct filter mapping set.
   */
  protected function translateParameters(array $parameters) {

    $filters = '';
    $fvalues = [];
    if (empty($this->xConnection)) {
      // No connection available.
      $this->messenger->addError('Translate parameters failed: no connection available.');
      return [$filters, $fvalues];
    }

    $conditions = [];
    $param_placeholder_index = 1;
    // Dummy table in order to get a SelectInterface object.
    $select = $this->xConnection->select('-DUMMY-');

    // $parameters field name are using the form "machineName" while
    // getFilterMapping() is using the form "field_machine_name
    foreach ($parameters as &$parameter) {
      // Replace entity field name with associated SQL column name or
      // expression.
      if ($real_name = $this->getFilterMapping($parameter['field'])) {
        $parameter['field'] = $real_name;
      }
      else {
        // This cannot be guessed.
        $this->log('Field "' . $parameter['field'] . '" real name in SQL query cannot be guessed for filtering.');
        continue;
      }

      // Prepare values.
      $param_placeholder = ':xnttdbtparam' . $param_placeholder_index++;
      if (is_array($parameter['value'])) {
        $param_placeholder .= '[]';
      }
      $fvalues[$param_placeholder] = $parameter['value'];

      // Translate non-SQL conditions such as CONTAINS.
      Condition::translateCondition($parameter, $select, FALSE);

      // Build SQL condition strings.
      if ($parameter['operator'] === 'LIKE'
          && ($this->xConnection->databaseType() === 'pgsql')
      ) {
        // Deal with capitalization.
        $parameter['operator'] = 'ILIKE';
      }
      if ($parameter['operator'] == 'IN') {
        $param_placeholder = '(' . $param_placeholder . ')';
      }
      $conditions[] = $parameter['field'] . ' ' . $parameter['operator'] . ' ' . $param_placeholder;
    }

    $filters = empty($conditions) ? '' : 'AND ' . implode(' AND ', $conditions);
    return [$filters, $fvalues];
  }

  /**
   * Get SQL column name or expression associated with a field.
   *
   * @param string $field_machine_name
   *   Field machine name (ie. "field_machine_name" and not "machineName").
   *
   * @return string
   *   SQL column name or expression.
   */
  protected function getFilterMapping($field_machine_name) {
    // Get mapping from filter configuration.
    $field_mapping =
      $this->configuration['filter_mappings'][$field_machine_name]
      ?? ''
    ;

    if (empty($field_mapping)) {
      // Not set, fallback to default mapping.
      $field_mapper = $this->externalEntityType->getFieldMapper();
      $field_mapping =
        $field_mapper->getFieldMapping($field_machine_name)['value']
        ?? ''
      ;
      // Check if the field name corresponds to an alias and not a real column.
      // This regex is not perfect but may help in many cases.
      if (!empty($field_mapping)
          && preg_match('/(?:SELECT\s|,\s*)([^,\s]+)\s+(?:AS\s|)(?:"\Q' . $field_mapping . '\E"|\Q' . $field_mapping . '\E\W)/is', $this->configuration['qread'], $match)
      ) {
        // We got an expression, try to use it instead of the alias.
        $field_mapping = $match[1];
      }
    }
    return $field_mapping;
  }

  /**
   * {@inheritdoc}
   */
  public function countQuery(array $parameters = []) {

    if (empty($this->xConnection)) {
      // No connection available.
      $this->messenger->addError('Count query failed: no connection available.');
      return;
    }

    $count = 0;
    try {
      list($filters, $fvalues) = $this->translateParameters($parameters);
      $query = rtrim($this->configuration['qcount'], ';');
      if (empty($filters)) {
        // No filters, remove any empty 'WHERE' structure.
        $query = preg_replace('/\sWHERE\s+(?:TRUE\s+)?:filters\s*$/is', '', $query);
      }
      else {
        // Check if the query contains the ":filters" placeholder.
        if (FALSE === strpos($query, ':filters')) {
          // No ":filters", try to append.
          if (preg_match('/\sWHERE\s/is', $query)) {
            $query .= ' :filters';
          }
          else {
            $query .= ' WHERE :filters';
          }
        }
      }
      $query = str_replace(':filters', $filters, $query);
      $qargs = $this->getPlaceholders($query);
      if (!empty($this->configuration['debug'])) {
        $this->log("COUNT SQL Query:\n$query\n\nArguments:\n" . print_r($fvalues + $qargs, TRUE));
      }
      $count = $this->xConnection
        ->query($query, $fvalues + $qargs)
        ->fetch()
        ->count
      ;
    }
    catch (DatabaseExceptionWrapper $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }
    catch (IntegrityConstraintViolationException $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }

    return $count;
  }

  /**
   * Handles query placeholders and perform a set of SQL queries.
   *
   * @param string $queries
   *   A set of queries separated by ';'.
   * @param array $entity_data
   *   Entity fields as an associative array.
   *
   * @return array
   *   An empty array if a query failed and non-empty array otherwise. The array
   *   will contain an aggregation of query results keyed by field names. In
   *   case of multiple queries returning the same field name, only the last
   *   value will be kept. If no fields are returned, the array will just
   *   contain the "TRUE" value.
   */
  protected function runQueries(string $queries, array $entity_data) :array {

    if (empty($this->xConnection)) {
      // No connection available.
      $this->messenger->addError('Failed to run queries: no connection available.');
      return [];
    }

    // Start a new transaction.
    $transaction = $this->xConnection->startTransaction();
    $return = [];
    try {
      // @todo better parse ';' as it can be used in query strings.
      $sql_queries = explode(';', $queries);
      // Loop on queries.
      foreach ($sql_queries as $sql_query) {
        $sql_query = trim($sql_query);
        if (empty($sql_query)) {
          continue;
        }
        $values = [];
        // Get the list of field values needed.
        preg_match_all(
          '/\W:([a-z_]\w*(?:\[\])?)/i',
          $sql_query,
          $placeholders,
          PREG_PATTERN_ORDER
        );
        $placeholders = array_flip($placeholders[1]);
        foreach ($placeholders as $placeholder => $waste) {
          // Check for array placeholder and array values.
          if ('[]' == substr($placeholder, -2)) {
            $placeholder_name = substr($placeholder, 0, -2);
            if (array_key_exists($placeholder_name, $entity_data)) {
              if (is_array($entity_data[$placeholder_name])) {
                $values[":$placeholder"] = $entity_data[$placeholder_name];
              }
              else {
                $values[":$placeholder"] = [$entity_data[$placeholder_name]];
              }
            }
          }
          elseif (array_key_exists($placeholder, $entity_data)) {
            if (is_array($entity_data[$placeholder])) {
              $values[":$placeholder"] = current($entity_data[$placeholder]);
              $this->logger->warning('The entity field placeholder ":' . $placeholder . '" maps for an array of values while it does not end with "[]"');
            }
            else {
              $values[":$placeholder"] = $entity_data[$placeholder];
            }
          }
        }
        // Get placeholders.
        $qargs = $this->getPlaceholders($sql_query);
        if (!empty($this->configuration['debug'])) {
          $this->log("SQL Query:\n$sql_query\n\nArguments:\n" . print_r($values + $qargs, TRUE));
        }
        $result = $this->xConnection->query(
          $sql_query,
          $values + $qargs
        )->fetch(\PDO::FETCH_ASSOC);
        if (is_array($result)) {
          foreach ($result as $key => $value) {
            if (0 === strpos($key, 'array_')) {
              // Removes leading '{' and trailing '}' and explode string on ','.
              $return[$key] = $this->expandDatabaseArray($value);
            }
            elseif (0 === strpos($key, 'json_')) {
              $info = "(field:$key)";
              $return[$key] = $this->expandDatabaseJson($value);
            }
            else {
              $return[$key] = $value;
            }
          }
        }
      }
      // Make sure we return a non-empty array on success.
      if (empty($return)) {
        $return = [TRUE];
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $transaction->rollBack();
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }
    catch (IntegrityConstraintViolationException $e) {
      $this->messenger->addError('Query failed: ' . $e->getMessage());
      $this->logger->error($e);
    }

    // Commit.
    unset($transaction);
    return $return;
  }

  /**
   * Initializes placeholder replacement.
   *
   * @param bool $reload_cache
   *   If TRUE, it reloads identifier cache.
   *
   * @return array
   *  Returns the list of placeholders keyed by external entity type, then by
   *  placeholder name with their associated values.
   */
  public function initPlaceholders(bool $reload_cache = FALSE) :array {

    // Get cache.
    $cid = 'xnttdb:placeholders';
    $placeholders = [];
    if ($cache = $this->cache->get($cid)) {
      $placeholders = $cache->data;
    }

    // Checks if the external entity type's mapping has already been processed.
    if ($this->externalEntityType) {
      $entity_type = $this->externalEntityType->getDerivedEntityTypeId();
      if (!isset($placeholders[$entity_type])) {
        $reload_cache = TRUE;
      }
    }
    else {
      $entity_type = '';
      $reload_cache = TRUE;
    }

    // Load placeholder replacement values.
    if ($reload_cache && $this->xConnection) {
      $placeholders_issues = FALSE;
      $placeholders[$entity_type] = [];
      // Process the list of placeholders from config.
      foreach ($this->configuration['placeholders'] as $ph_def) {
        if (empty($ph_def) || empty($ph_def['placeholder'])) {
          continue;
        }
        // Skip placeholders not starting with ':'.
        if (!preg_match('/^:[a-z]\w*(?:\[\])?$/i', $ph_def['placeholder'])) {
          $placeholders_issues = TRUE;
          $this->logger->warning('Ignoring invalid placeholder name "' . $ph_def['placeholder'] . '"');
          continue;
        }
        // Check for query or constant values.
        if (!empty($ph_def['query'])) {
          // Process placeholder query.
          try {
            if (!empty($this->configuration['debug'])) {
              $this->log("Placeholder SQL Query:\n" . $ph_def['query']);
            }
            $results = $this->xConnection->query(
              $ph_def['query']
            )->fetchAll(\PDO::FETCH_ASSOC);
            if (!empty($results)) {
              // Save placeholder replacement value.
              if ('[]' === substr($ph_def['placeholder'], -2)) {
                $placeholders[$entity_type][$ph_def['placeholder']] = array_map(
                  function ($x) {
                    return $x[array_key_first($x)];
                  },
                  $results
                );
              }
              else {
                $placeholders[$entity_type][$ph_def['placeholder']] =
                  $results[0][array_key_first($results[0])];
              }
            }
            else {
              $this->messenger->addWarning(
                'Failed to get value(s) for placeholder "' . $ph_def['placeholder'] . '". Query "' . $ph_def['query'] . '" returned no value.'
              );
            }
          }
          catch (DatabaseExceptionWrapper $e) {
            $this->messenger->addWarning(
              'Failed to get value(s) for placeholder "' . $ph_def['placeholder'] . '" (database error). '
              . $e->getMessage()
            );
          }
          catch (IntegrityConstraintViolationException $e) {
            $this->messenger->addWarning(
              'Failed to get value(s) for placeholder "' . $ph_def['placeholder'] . '" (constraint error). '
              . $e->getMessage()
            );
          }
          catch (\Throwable $e) {
            $this->messenger->addWarning(
              'Failed to get value(s) for placeholder "' . $ph_def['placeholder'] . '". '
              . $e->getMessage()
            );
          }
        }
        elseif (isset($ph_def['constant'])) {
          // Save placeholder replacement value.
          if ('[]' === substr($ph_def['placeholder'], -2)) {
            if (is_array($ph_def['constant'])) {
              $placeholders[$entity_type][$ph_def['placeholder']] =
                $ph_def['constant'];
            }
            elseif (is_string($ph_def['constant'])) {
              $placeholders[$entity_type][$ph_def['placeholder']] =
                preg_split(
                  '/"\s*,\s*(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"/',
                  $ph_def['constant']
                )
              ;
            }
            else {
              $placeholders[$entity_type][$ph_def['placeholder']] =
                [$ph_def['constant']];
            }
          }
          else {
            $placeholders[$entity_type][$ph_def['placeholder']] =
              $ph_def['constant'];
          }
        }
        else {
          $placeholders_issues = TRUE;
          $this->logger->warning('Ignoring invalid placeholder settings for placeholder "' . $ph_def['placeholder'] . '"');
        }
      }
      if ($placeholders_issues) {
        $this->messenger->addWarning(
          'A couple of placeholder names were invalid and ignored. Please check the external entity configuration and see logs for details.'
        );
      }
      $this->cache->set($cid, $placeholders);
    }

    return $this->placeholders = $placeholders;
  }

  /**
   * Sets the value of a placeholder.
   *
   * @param string $name
   *   Placeholder name including its ':' prefix (and possible trailing '[]').
   * @param $value
   *   Value to use.
   */
  public function setPlaceholder(string $name, $value) {
    $entity_type = '';
    if ($this->externalEntityType) {
      $entity_type = $this->externalEntityType->getDerivedEntityTypeId();
    }
    // Init Placeholders if missing and get them.
    $this->placeholders[$entity_type] = $this->placeholders[$entity_type] ?? [];
    $this->placeholders[$entity_type][$name] = $value;
  }

  /**
   * Returns an array of placeholders for current entity type.
   *
   * @param ?string $query
   *   An SQL query containing placeholders of the form ':placeholdername'.
   *   If not empty, only placeholders matching the query will be returned.
   *   If empty, all placeholders are returned.
   *
   * @return array
   *   The array of placeholders as placeholder_name => placeholder_value.
   *   Note: placeholder names start with ':' and may end with '[]' for arrays
   *   and placeholder values may be strings or arrays.
   */
  public function getPlaceholders(?string $query = NULL) :array {
    $entity_type = '';
    if ($this->externalEntityType) {
      $entity_type = $this->externalEntityType->getDerivedEntityTypeId();
    }
    // Init Placeholders if missing and get them.
    $this->placeholders[$entity_type] = $this->placeholders[$entity_type] ?? [];

    $placeholders = [];
    if (!empty($query)) {
      foreach ($this->placeholders[$entity_type] as $placeholder => $ph_value) {
        if (preg_match('/\W\Q' . $placeholder . '\E(?:\W|$)/', $query)) {
          $placeholders[$placeholder] = $ph_value;
        }
      }
    }
    else {
      $placeholders = $this->placeholders[$entity_type];
    }

    return $placeholders;
  }

  /**
   * Replace placeholders in a query.
   *
   * @param string $query
   *   An SQL query containing placeholders of the form ':placeholdername'.
   * @param array $additional_replacements
   *   Additional replacement patterns, such as :filters.
   *
   * @return string
   *   The query containing actual replacement values.
   */
  public function replacePlaceholders(string $query, array $additional_replacements = []) :string {
    $placeholders = $this->getPlaceholders() + $additional_replacements;
    array_walk(
      $placeholders,
      function (&$item, $key) {
        if (is_array($item)) {
          $item = implode(', ', $item);
        }
      }
    );
    // Reverse sort by key to replace longuest placeholders first and avoid
    // having a part of a placeholder replaced by another.
    // ex.: ':abc' with value 42 and ':abcdef' with value 806. If we replace
    // first ':abc', a placeholder ':abcdef' would be turned into "42def"
    // instead of "806".
    // We could use regex replacement, which might be less efficient but more
    // secure.
    krsort($placeholders);

    $query = str_replace(
      array_keys($placeholders),
      array_values($placeholders),
      $query
    );
    return $query;
  }

  /**
   * Logs message in Drupal logs.
   *
   * @param string $message
   *   Message to log.
   */
  protected function log(string $message, string $mode = 'debug') {
    switch ($mode) {
      case 'emergency':
        $this->logger->emergency(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'alert':
        $this->logger->alert(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'critical':
        $this->logger->critical(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'error':
        $this->logger->error(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'warning':
        $this->logger->warning(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'notice':
        $this->logger->notice(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'info':
        $this->logger->info(
          $this->preprocessLogMessage($message)
        );
        break;

      case 'debug':
      default:
        $this->logger->debug(
          $this->preprocessLogMessage($message)
        );
        break;
    }
  }

  /**
   * Replace special log characters to avoid invalid log replacements.
   *
   * When using Drupal logger (PSR-3), it tries to replace groups of curly
   * braces "{}" and square brackets "[]" by substitution strings, even if there
   * are not available, leading ot incorrect log message.
   * To avoid that, we replace curly braces and square brakets with thier
   * equivalent UTF8 characters that won't be processed by logger replacement.
   *
   * @param string $message
   *   The log message.
   *
   * @return string
   *   The adjusted corresponding log message.
   */
  protected function preprocessLogMessage(string $message) :string {
    return str_replace(['{', '}', '[', ']'], ["\xEF\xBD\x9B", "\xEF\xBD\x9D", "\xEF\xBC\xBB", "\xEF\xBC\xBD"], $message);
  }

}
