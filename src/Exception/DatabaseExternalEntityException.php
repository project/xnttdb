<?php

namespace Drupal\xnttdb\Exception;

/**
 * Exception thrown by External Entities Database Storage Client.
 */
class DatabaseExternalEntityException extends \RuntimeException {}
